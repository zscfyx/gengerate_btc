# gengerate_btc

#### 联系方式：

> 公众号：区块链投资宝典

#### 介绍：破解btc的第100种方法


> 批量生成 BTC 助记词，私钥和地址，并与富豪地址（持币数很多的地址）对比

> 程序汉化于 Github 上E神的作品，仅用于免费研究与测试，请勿用于非法用途


#### 运行原理：

1.  程序循环生成币种的助记词，私钥和地址  （下文：协议及接口）
2.  将生成的地址与富豪地址进行对比  （下文：协议及接口）
3.  如果地址相同，将助记词导入到钱包，即可进行转账  （下文：去中心化钱包）


#### 下载地址：

目前只有 windows 版本


[点击下载](https://fengyunxiao.oss-cn-shanghai.aliyuncs.com/generate/generate.zip)


#### 安装教程

1.  该程序使用 javafx 开发，打包文件很大，默认打包了 jre，不安装 jdk 也可以使用。
2.  下载 zip 文件后，解压到任意盘。目录最好不要带中文。
3.  运行目录里面的 generate.exe


#### 截图：

目录截图

![目录截图](https://fengyunxiao.oss-cn-shanghai.aliyuncs.com/generate/p1.png "在这里输入图片标题")


主界面截图

![主界面截图](https://fengyunxiao.oss-cn-shanghai.aliyuncs.com/generate/p2.png "在这里输入图片标题")

#### 协议及接口


|  名称 |  链接 |
|---|---|
|  BIP 39协议：2048个助记词 | [BIP39协议 链接](https://github.com/bitcoin/bips/blob/master/bip-0039/english.txt)  |
|  BIP 44协议：确定性钱包的逻辑层次结构 | [BIP 44协议 链接](https://github.com/satoshilabs/slips/blob/master/slip-0044.md)  |
|  富豪地址接口1 |  [富豪地址接口1 链接](https://tokenview.com/cn/topaccount/btc) |
|  富豪地址接口2 |  [富豪地址接口2 链接](https://www.okcoin.cn/btc/rich-list) |
|  区块浏览器接口1 |  [区块浏览器接口1 链接](https://bch.btc.com/api-doc) |
|  区块浏览器接口2 |  [区块浏览器接口2 链接](https://btc.com/api-doc) |


#### 去中心化钱包


| 钱包名称  |  支持的币种 |  不支持的币种 |
|---|---|---|
|  火币钱包 | btc bch eth ltc dash  |   |
|  jaxx钱包 |  btc bch eth ltc dash |   |
|  imtoken2 |  bch eth ltc |  btc（3开头的多签地址） |
|  trustwallet |  bch eth dash |  btc（bip84协议，隔离见证地址，bc1  开头 ） |
|              |               |  ltc（bip84协议，隔离见证地址，ltc1 开头）|     
|  atoken钱包 |  eth |  btc（3开头的多签地址） |


优先推荐：火币钱包 和 jaxx钱包，其他 imtoken。trustwallet 比较超前，支持的协议较新。